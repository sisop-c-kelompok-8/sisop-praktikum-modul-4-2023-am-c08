# PRAKTIKUM 3 KELOMPOK 8 SISOP C


### Praktikum Sisop Modul 3
Group Members:
1. Keysa Anadea (5025211028)
2. Anneu Tsabita (5025211026)
3. Dimas Pujangga (5025211212)

## Question 1  
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.
**Catatan: **
- Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
- Perhatikan port  pada masing-masing instance.

#### 1A    
**_SOAL :_**    
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

**_Pengaplikasian Pada Program:_**    

```c
int main() {
    int status;
    pid_t child_id;

    child_id = fork();

    if (child_id < 0) {
        printf("Failed to create a new process.\n");
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        // This is the child process
        char *downloadCommand[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
        execvp("kaggle", downloadCommand);
        exit(EXIT_FAILURE);
    }

    waitpid(child_id, &status, 0);

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        child_id = fork();

        if (child_id < 0) {
            printf("Failed to create a new process.\n");
            exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
            // This is the child process
            char *extractCommand[] = {"unzip", "fifa-player-stats-database.zip", NULL};
            execvp("unzip", extractCommand);
            exit(EXIT_FAILURE);
        }

        waitpid(child_id, &status, 0);

```
**_Penjelasan Kode:_**          

1. Deklarasi Variabel:
```c
int status;
pid_t child_id;
```
Variabel status akan digunakan untuk menyimpan status keluaran proses anak, dan variabel child_id akan menyimpan ID proses anak yang dibuat.

2. fork():
```c
child_id = fork();

if (child_id < 0) {
    printf("Failed to create a new process.\n");
    exit(EXIT_FAILURE);
}
```
fork() digunakan untuk membuat proses anak baru. Jika nilai yang dikembalikan oleh fork() kurang dari 0, itu berarti gagal membuat proses anak. Dalam hal ini, program mencetak pesan kesalahan dan keluar dari program menggunakan exit(EXIT_FAILURE).

3. Proses Anak Pertama:
```c
if (child_id == 0) {
    // This is the child process
    char *downloadCommand[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
    execvp("kaggle", downloadCommand);
    exit(EXIT_FAILURE);
}
```
Jika nilai yang dikembalikan oleh fork() adalah 0, itu berarti proses ini adalah proses anak. Dalam blok ini, program menggunakan execvp() untuk menjalankan perintah "kaggle datasets download -d bryanb/fifa-player-stats-database". Perintah ini digunakan untuk mengunduh dataset pemain FIFA. Jika execvp() gagal, pesan kesalahan dicetak dan proses anak keluar menggunakan exit(EXIT_FAILURE).

4. waitpid():
```c
waitpid(child_id, &status, 0);
```
Setelah proses anak pertama selesai, proses induk menunggu proses anak selesai menggunakan waitpid(). waitpid() akan menghentikan eksekusi proses induk sampai proses anak selesai.

5. Proses Anak Kedua:
```c
if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
    child_id = fork();

    if (child_id < 0) {
        printf("Failed to create a new process.\n");
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        // This is the child process
        char *extractCommand[] = {"unzip", "fifa-player-stats-database.zip", NULL};
        execvp("unzip", extractCommand);
        exit(EXIT_FAILURE);
    }

    waitpid(child_id, &status, 0);
}
```
Setelah menunggu proses anak pertama selesai, program memeriksa apakah proses anak pertama keluar dengan status keluaran yang normal (exit status 0). Jika ya, maka blok kode berikutnya akan dijalankan. Dalam blok ini, proses anak kedua akan dibuat menggunakan fork(), dan kemudian execvp() akan digunakan untuk menjalankan perintah "unzip fifa-player-stats-database.zip". Jika execvp() gagal, pesan kesalahan dicetak dan proses anak keluar menggunakan exit(EXIT_FAILURE). Setelah itu, proses induk menunggu proses anak kedua selesai menggunakan waitpid().

#### 1B   
**_SOAL :_**    
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

**_Pengaplikasian Pada Program:_**     
```c
 system("awk -F\",\" '{if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\") print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\"}' FIFA23_official_data.csv");

```
**_Penjelasan Kode:_**  
1. `system()`:      
system() adalah fungsi dalam bahasa C yang digunakan untuk menjalankan perintah shell. Dalam kasus ini, perintah shell yang akan dieksekusi adalah perintah awk.

2. ` awk`:      
awk adalah sebuah utilitas di lingkungan Unix yang berguna untuk memanipulasi dan memproses data dalam format teks. Dalam kode ini, awk digunakan untuk memproses file FIFA23_official_data.csv.

3. if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\"):        
Ini adalah kondisi if dalam awk yang akan dievaluasi untuk setiap baris dalam file CSV. Jika kondisi ini benar, maka instruksi di dalam blok if akan dijalankan.

4. print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\":       
Ini adalah instruksi print dalam awk yang akan mencetak output yang diformat ke layar. Output akan berisi informasi seperti nama pemain, klub, usia, potensi, foto, dan kewarganegaraan, sesuai dengan kondisi yang ditentukan sebelumnya.

#### 1C
**_SOAL :_**    
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

**_Pengaplikasian Pada Program:_**      
```dockerfile
# Use base image
FROM ubuntu:20.04

# Install any required dependencies or packages
RUN apt-get update && \
    apt-get install -y gcc unzip python3-pip && \
    pip install kaggle

# Download the Kaggle API credentials file (kaggle.json) and move it to the appropriate location
COPY kaggle.json /root/.kaggle/kaggle.json

# Set the permissions for the Kaggle API credentials file
RUN chmod 600 /root/.kaggle/kaggle.json

# Install the unzip package
RUN apt-get install -y unzip

# Copy files into the container
COPY storage.c /app/storage.c

# Set working directory
WORKDIR /app

# Compile the storage.c file
RUN gcc -o storage storage.c

# Set an entrypoint script to execute the storage program and display the output
CMD ["./storage"]

```
**_Penjelasan Kode:_**   
1. Base Image:
```Dockerfile
FROM ubuntu:20.04
```
Baris ini menentukan base image yang digunakan untuk membangun image Docker. Dalam hal ini, menggunakan base image Ubuntu 20.04.        

2. Install Dependencies:
```Dockerfile
RUN apt-get update && \
    apt-get install -y gcc unzip python3-pip && \
    pip install kaggle
```
Baris ini menggunakan perintah RUN untuk menjalankan beberapa perintah dalam container. Perintah ini akan melakukan update package manager, menginstal gcc, unzip, dan python3-pip, serta menginstal package kaggle menggunakan pip.      

3. Copy Kaggle API Credentials:
```Dockerfile
COPY kaggle.json /root/.kaggle/kaggle.json
```
Baris ini menggunakan perintah COPY untuk menyalin file kaggle.json dari direktori lokal ke direktori /root/.kaggle/kaggle.json dalam container. File ini berisi kredensial API Kaggle yang diperlukan untuk mengakses Kaggle API.      

4. Set Permissions for Kaggle API Credentials:
```Dockerfile
RUN chmod 600 /root/.kaggle/kaggle.json
```
Baris ini menggunakan perintah RUN untuk menjalankan perintah chmod dalam container. Perintah ini mengatur izin file kaggle.json menjadi 600 (hanya pemilik yang dapat membaca dan menulis file tersebut.   

5. Install Unzip Package:
```Dockerfile
RUN apt-get install -y unzip
```
Baris ini menggunakan perintah RUN untuk menginstal package unzip menggunakan package manager di dalam container.     

6. Copy Files into the Container:
```Dockerfile
COPY storage.c /app/storage.c
```
Baris ini menggunakan perintah COPY untuk menyalin file storage.c dari direktori lokal ke direktori /app/storage.c dalam container. File storage.c adalah file program dalam bahasa C yang akan digunakan dalam konteks ini.      

7. Set Working Directory:
```Dockerfile
WORKDIR /app
```
Baris ini menggunakan perintah WORKDIR untuk mengatur direktori kerja saat ini menjadi /app. Ini akan menjadi direktori aktif ketika perintah selanjutnya dijalankan.     

8. Compile the storage.c File:
```Dockerfile
RUN gcc -o storage storage.c
```
Baris ini menggunakan perintah RUN untuk menjalankan perintah gcc dalam container. Perintah ini akan mengkompilasi file storage.c menjadi file eksekusi storage dalam direktori kerja /app.     

9. Set Entrypoint Script:
```Dockerfile
Copy code
CMD ["./storage"]
```
Baris ini menggunakan perintah CMD untuk menentukan perintah default yang akan dijalankan saat container dijalankan. Dalam hal ini, perintah ./storage akan dijalankan, yang akan menjalankan program storage yang telah dikompilasi sebelumnya.

#### 1D
**_SOAL :_**    
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

**_Pengaplikasian Pada Program:_**    
![image](/uploads/e807e2946e9f3c0f98b14ab065ead641/image.png)
![image](/uploads/ce7090bbd3a246377e1767e7622dd181/image.png)

#### 1E
**_SOAL :_**    
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

**_Pengaplikasian Pada Program:_**      
```dockerfile
version: '3'
services:
#instance 1
  1.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8080:80"
#instance 2
  2.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8081:80"
  #instance 3
  3.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8082:80"
  #instance 4
  4.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8083:80"
  #instance 5
  5.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8084:80"


```

#### Source Code Question 1
**storage.c:**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
    int status;
    pid_t child_id;

    child_id = fork();

    if (child_id < 0) {
        printf("Failed to create a new process.\n");
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        // This is the child process
        char *downloadCommand[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
        execvp("kaggle", downloadCommand);
        exit(EXIT_FAILURE);
    }

    waitpid(child_id, &status, 0);

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        child_id = fork();

        if (child_id < 0) {
            printf("Failed to create a new process.\n");
            exit(EXIT_FAILURE);
        }

        if (child_id == 0) {
            // This is the child process
            char *extractCommand[] = {"unzip", "fifa-player-stats-database.zip", NULL};
            execvp("unzip", extractCommand);
            exit(EXIT_FAILURE);
        }

        waitpid(child_id, &status, 0);

        system("awk -F\",\" '{if ($3 < 25 && $8 > 85 && $9 != \"Manchester City\") print \"Name :\",$2,\"\\n Club: \",$9,\"\\n Age: \",$3,\"\\n Potential: \",$8,\"\\n Photo: \",$4,\"\\n Nationality: \",$5,\"\\n\"}' FIFA23_official_data.csv");
    }

    return 0;
}
```

**dockerfile:**
```dockerfile
# Use base image
FROM ubuntu:20.04

# Install any required dependencies or packages
RUN apt-get update && \
    apt-get install -y gcc unzip python3-pip && \
    pip install kaggle

# Download the Kaggle API credentials file (kaggle.json) and move it to the appropriate location
COPY kaggle.json /root/.kaggle/kaggle.json

# Set the permissions for the Kaggle API credentials file
RUN chmod 600 /root/.kaggle/kaggle.json

# Install the unzip package
RUN apt-get install -y unzip

# Copy files into the container
COPY storage.c /app/storage.c

# Set working directory
WORKDIR /app

# Compile the storage.c file
RUN gcc -o storage storage.c

# Set an entrypoint script to execute the storage program and display the output
CMD ["./storage"]

```
**docker-compose.yml:**
```docker-compose.yml
version: '3'
services:
#instance 1
  1.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8080:80"
#instance 2
  2.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8081:80"
  #instance 3
  3.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8082:80"
  #instance 4
  4.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8083:80"
  #instance 5
  5.docker:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8084:80"
```
#### Output Question 1
**storage.c:**
![image](/uploads/7b80b2ca82152fbb8e296cae08a82bc2/image.png)
![image](/uploads/b94fd8bc923e2a11df1c0195d63b415f/image.png)
**dockerfile:**
![image](/uploads/bf5ccac1ee2d02b74c0bb60e71175916/image.png)
![image](/uploads/f1721b922150182f6b145cf6dd6dc5e9/image.png)
**docker-compose.yml:**
![image](/uploads/c05fb727a30b347dc890cfca8cb815f5/image.png)
![image](/uploads/983a627900f5625ed53eff9051523522/image.png)
![image](/uploads/f2388d2f938cfa34ae3ba7293d1a0da7/image.png)


## Question 4  
Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. _Bank_ tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya _maintenance_, petinggi _bank_ ini menyewa seseorang yang mampu mengorganisir _file-file_ yang ada di Bank Sabar Indonesia. 
Salah satu karyawan di _bank_ tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi _file-file_ yang ada dalam bentuk **modular**, yaitu membagi file yang mulanya berukuran besar menjadi _file-file chunk_ yang berukuran kecil. Hal ini bertujuan agar saat terjadi error, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di _filesystem_, Bagong membuat sebuah sistem **log** untuk mempermudah _monitoring_ kegiatan di _filesystem_ yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah _file log_ dengan ketentuan sebagai berikut. 
- Log akan dibagi menjadi beberapa level, yaitu **REPORT** dan **FLAG**.
- Pada level log **FLAG**, log akan mencatat setiap kali terjadi system call **rmdir** (untuk menghapus direktori) dan **unlink** (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log **REPORT**.
- Format untuk logging yaitu sebagai berikut.

    ```[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]```

    **Contoh:**

    REPORT::230419-12:29:28::RENAME::/bsi23/bagong.jpg::/bsi23/bagong.jpeg

    REPORT::230419-12:29:33::CREATE::/bsi23/bagong.jpg

    FLAG::230419-12:29:33::RMDIR::/bsi23


Tidak hanya itu, Bagong juga berpikir tentang beberapa hal yang berkaitan dengan ide modularisasinya sebagai berikut yang ditulis dalam **modular.c**.
- Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan **module_**, maka direktori tersebut akan menjadi direktori modular. 

- Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.

- Sebuah file nantinya akan terbentuk bernama **fs_module.log** pada direktori home pengguna (**/home/[user]/fs_module.log**) yang berguna menyimpan daftar perintah **system call** yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.

- Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.

    **Contoh:**

    file **File_Bagong.txt** berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni **File_Bagong.txt.000**, **File_Bagong.txt.001**, dan **File_Bagong.txt.002** yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

- Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

#### Penyelesaian   

#### 1. Cek Nama path
Dalam sistem FUSE, semua berkas di direktori yang disebut FUSE serta semua subdirektorinya akan diubah menjadi modular. Untuk itu, kita memakai fungsi strcmp untuk membandingkan jalur keseluruhan direktori dengan kode "module_". Jika jalur tersebut mengandung kode "module_", ini berarti bahwa direktori tersebut adalah direktori "module_" atau berada di dalam direktori "module_". Berikut adalah cara penerapannya:

```
char kode[10] = "module_";

...

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){ //baca directory
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

...

	while ((dir = readdir(dp)) != NULL) { //buat loop yang ada di dalem directory
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if(enc2 != NULL){
			encrypt(dir->d_name, enc2);
        } else {
			char *extension = strrchr(dir->d_name, '.');
            if (extension != NULL && strcmp(extension, ".001") == 0) {
                *extension = '\0';
                decode(newPath, dir->d_name);
            }
		}
		res = (filler(buf, dir->d_name, &st, 0));
		if(res!=0) break;
	}

	closedir(dp);
	return 0;
}

```
Kode tersebut ditempatkan dalam xmp_readdir() karena tujuannya adalah untuk menunjukkan kondisi file saat dibaca dan menentukan apakah file tersebut akan diubah menjadi modular atau tidak. Jika file akan diubah menjadi modular, fungsi encrypt() akan dijalankan, sedangkan jika file tidak dimodularisasi, fungsi decode() akan dieksekusi.

#### 2. Menulis Log
Log ditulis pada file fs_module.log pada direktori home user. Dengan format :

- [LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]

Untuk mengerjakan bagian log maka yang perlu dilakukan adalah menuliskan ke log sesuai dengen fungsi fuse yang diinginkan. Misalkan untuk rename maka pada bagian xmp_rename akan ditambahkan logsystem() yang gunanya untuk menulis log setiap fungsi dipanggil. Berikut adalah fungsi logsystem() :

```
void logSystem(char* c, int type){
    FILE * logFile = fopen("/home/anneutsabita/SISOP/praktikum4/soal4/fs_module.log", "a");
	time_t currTime;
	struct tm * time_info;
	time ( &currTime );
	time_info = localtime (&currTime);
    int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;
    if(type==1){//report type
        fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    else if(type==2){ //flag type
        fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    fclose(logFile);
}
```

Berikut adalah penjelasan dari setiap bagian dalam fungsi tersebut:

- void logSystem(char* c, int type): Fungsi ini menerima dua parameter, yaitu c yang merupakan string yang akan dicatat dalam log, dan type yang menentukan jenis catatan yang akan dibuat.

- FILE * logFile = fopen("/home/anneutsabita/SISOP/praktikum4/soal4/fs_module.log", "a"): Fungsi fopen digunakan untuk membuka file fs_module.log dalam mode "a" (append). Hal ini berarti file akan dibuka untuk penulisan pada akhir file tanpa menghapus konten yang sudah ada. File tersebut akan diwakili oleh pointer logFile.

- time_t currTime; struct tm * time_info; time ( &currTime ); time_info = localtime (&currTime);: Variabel currTime bertipe time_t digunakan untuk menyimpan waktu saat ini dalam bentuk UNIX timestamp. Selanjutnya, variabel time_info bertipe struct tm* digunakan untuk menyimpan informasi waktu yang diuraikan, yaitu tahun, bulan, tanggal, jam, menit, dan detik. Fungsi time dan localtime digunakan untuk mendapatkan waktu lokal saat ini.

- int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;: Mengambil informasi waktu yang telah diuraikan dari time_info dan menyimpannya dalam variabel terpisah. Variabel yr menyimpan tahun, mon menyimpan bulan, day menyimpan tanggal, hour menyimpan jam, min menyimpan menit, dan sec menyimpan detik.

- if(type==1): Melakukan pengecekan tipe catatan yang akan dibuat. Jika type bernilai 1, maka ini merupakan catatan jenis "report".

- fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c): Menggunakan fungsi fprintf untuk menulis catatan ke file fs_module.log. Catatan ini memiliki format yang ditentukan, yaitu "REPORT::YYYYMMDD-HH:MM:SS::<perintah>". Format tanggal dan waktu diisi dengan nilai-nilai yang telah diambil sebelumnya, dan <perintah> diisi dengan string c.

- else if(type==2): Jika type bernilai 2, maka ini merupakan catatan jenis "flag".

- fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c): Sama seperti sebelumnya, menggunakan fprintf untuk menulis catatan ke file fs_module.log. Catatan ini memiliki format "FLAG::YYYYMMDD-HH:MM:SS::<perintah>", dengan format tanggal, waktu, dan <perintah> diisi dengan nilai-nilai yang sesuai.

- fclose(logFile): Setelah selesai menulis catatan, file fs_module.log ditutup menggunakan fclose untuk mengosongkan buffer dan memastikan catatan tersimpan dengan baik.

Adapun pemanggilan pada setiap fungsi FUSE nya adalah sebagai berikut :

Membuat directory baru :
```
static int xmp_mkdir(const char *path, mode_t mode){ //buat bikin directory baru

    ...

    char str[100];
	sprintf(str, "MKDIR::%s", path);
	logSystem(str,1);
	
    ...

	return 0;
}
```

Membuat File baru :
```
static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){//buat file

	...

	logSystem(str,1);
	
    ...
	return 0;
}
```

Menghapus File :
```
static int xmp_unlink(const char *path) { //ngehapus file
	
    ...

    char str[100];
	sprintf(str, "REMOVE::%s", path);
	logSystem(str,2);
	
    ...

	return 0;
}

```

Hapus Direktori :
```
static int xmp_rmdir(const char *path) {//ngehapus directory
	...

    char str[100];
	sprintf(str, "RMDIR::%s", path);
	logSystem(str,2);

    ...

	return 0;
}

```

Rename :
```
static int xmp_rename(const char *from, const char *to) { //buat renme
	
    ...

    char str[100];
	sprintf(str, "RENAME::%s::%s", from, to);
	logSystem(str,1);
	
    ...

	return 0;
}
```

Write in file :
```
static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { //write file
	
	...

    char str[100];
	sprintf(str, "WRITE::%s", path);
	logSystem(str,1);
	
    ...
	return res;
}
```

Dari implementasi diatas penulisan suffix log dilakukan pada function kemudian akan dipassing ke log dan ditulis ketika dalam function logsystem() bersamaan dengan prefixnya.

#### 3. Memecah File
Untuk memecah file digunakan fungsi encrypt() sebagai berikut :
```
void encrypt(char* enc1, char * enc2){
    if(strcmp(enc1, ".") == 0) return;
    if(strcmp(enc1, "..") == 0)return;
    
    int chunks=0, i, accum;

    char largeFileName[200];    //change to your path
    sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);
    printf("%s\n",largeFileName);

    char filename[260];//base name for small files.
    sprintf(filename,"%s.",largeFileName);
    //printf("%s\n",filename);

    char smallFileName[300];
    char line[1080];
    FILE *fp1, *fp2;
    long sizeFile = file_size(largeFileName);
    // printf("File size : %ld\n",sizeFile);
    
	if (sizeFile > 1025){
		chunks = sizeFile/CHUNK + 1;//ensure end of file
		// printf("%d",chunks);
		fp1 = fopen(largeFileName, "r");
		if(fp1)
		{
			char number[5];
			printf("Splitting\n");
			for(i=0;i<chunks;i++)
			{
				accum = 0;
				sprintf(number,"%03d",i+1);
				
				sprintf(smallFileName, "%s%s", filename, number);

				// Check if segment file already exists, skip if it does
				if (segment_exists(smallFileName)) {
					continue;
				}
				
				fp2 = fopen(smallFileName, "wb");
				if (fp2) {
					while (accum < CHUNK && !feof(fp1)) {
						size_t bytesRead = fread(line, 1, sizeof(line), fp1);
						size_t bytesToWrite = bytesRead;
						if (accum + bytesToWrite > CHUNK) {
							bytesToWrite = CHUNK - accum;
						}
						fwrite(line, 1, bytesToWrite, fp2);
						accum += bytesToWrite;
					}
					fclose(fp2);
				}
			}
			fclose(fp1);
			// Hapus file asli setelah pemecahan selesai
			if (remove(largeFileName) != 0) {
				printf("Gagal menghapus file asli: %s\n", largeFileName);
			} else {
				printf("File asli dihapus: %s\n", largeFileName);
			}
		}
	}
    printf("keluar\n");
}
```
Berikut adalah penjelasan dari setiap bagian dalam fungsi tersebut:

- int chunks=0, i, accum;: Variabel chunks digunakan untuk menyimpan jumlah file kecil yang akan dihasilkan setelah pemecahan file besar. Variabel i digunakan sebagai iterator dalam loop, dan variabel accum digunakan untuk melacak jumlah byte yang telah ditulis ke file kecil saat melakukan pemecahan.

- char largeFileName[200]; sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);: Membentuk path lengkap dari file besar dengan menggabungkan dirPath, enc2, dan enc1. Path ini akan digunakan untuk membaca file besar yang akan dipotong.

- char filename[260]; sprintf(filename,"%s.",largeFileName);: Membentuk base name untuk file-file kecil yang akan dihasilkan. Base name ini merupakan gabungan dari largeFileName dan tanda ".".

- char smallFileName[300]; char line[1080]; FILE *fp1, *fp2;: Variabel smallFileName digunakan untuk menyimpan nama file kecil yang akan dihasilkan dalam setiap iterasi. Variabel line digunakan untuk membaca dan menulis data dari file besar ke file kecil. Variabel fp1 dan fp2 adalah pointer yang akan digunakan untuk membuka file besar dan file kecil, masing-masing.

- long sizeFile = file_size(largeFileName);: Menghitung ukuran file besar dengan memanggil fungsi file_size. Hasilnya disimpan dalam variabel sizeFile.

- if (sizeFile > 1025){: Memeriksa apakah ukuran file besar melebihi batas minimum yang ditentukan (1025 byte). Jika ukuran file kurang dari atau sama dengan batas tersebut, maka pemecahan tidak perlu dilakukan.

- chunks = sizeFile/CHUNK + 1;: Menghitung jumlah file kecil yang akan dihasilkan berdasarkan ukuran file besar. Pembagian dilakukan dengan ukuran tetap yang ditentukan oleh konstanta CHUNK. Penambahan 1 dilakukan untuk memastikan bahwa seluruh data file besar tercakup dalam file-file kecil yang dihasilkan.

- fp1 = fopen(largeFileName, "r");: Membuka file besar dalam mode "r" (read). File ini akan dibaca untuk memecah datanya menjadi file-file kecil.

- char number[5]; sprintf(number,"%03d",i+1);: Membentuk angka yang akan digunakan dalam nama file kecil. Angka ini berupa nomor urut dengan tiga digit, dimulai dari 001.

- sprintf(smallFileName, "%s%s", filename, number);: Membentuk nama file kecil dengan menggabungkan base name filename dan angka number.

- if (segment_exists(smallFileName)) { continue; }: Memeriksa apakah file kecil dengan nama yang sama telah ada. Jika sudah ada, iterasi selanjutnya akan dilanjutkan dan file kecil baru tidak akan dibuat. Ini menghindari duplikasi file saat fungsi encrypt dipanggil ulang untuk file yang sama.

- fp2 = fopen(smallFileName, "wb");: Membuka file kecil dalam mode "wb" (write binary). File ini akan digunakan untuk menulis data yang dipotong dari file besar.

- while (accum < CHUNK && !feof(fp1)) { ... }: Melakukan loop untuk membaca data dari file besar dan menulisnya ke file kecil. Loop akan berlanjut selama accum (jumlah byte yang telah ditulis ke file kecil) belum mencapai ukuran tetap CHUNK dan belum mencapai akhir file besar.

- fwrite(line, 1, bytesToWrite, fp2);: Menulis bytesToWrite byte data ke file kecil dari buffer line. bytesToWrite adalah jumlah byte yang akan ditulis dalam satu iterasi.

- fclose(fp2);: Menutup file kecil setelah selesai menulis data.

- Setelah loop selesai, fclose(fp1); akan menutup file besar.

- if (remove(largeFileName) != 0) { ... }: Menghapus file asli setelah pemecahan selesai. Jika penghapusan gagal, pesan kesalahan akan ditampilkan. Jika berhasil, pesan berhasil dihapus akan ditampilkan.

#### 4. Merge File
Untuk menggabung file digunakan function decode():
```
void decode(const char *directoryPath, const char *prefix) {
    DIR *dir;
    struct dirent *entry;
    char filePath[1000];
    char mergedFilePath[1000];
    FILE *mergedFile;
    size_t prefixLength = strlen(prefix);

    dir = opendir(directoryPath);

    sprintf(mergedFilePath, "%s/%s", directoryPath, prefix);
    mergedFile = fopen(mergedFilePath, "w");

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) {
            sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
            FILE *file = fopen(filePath, "r");

			// Skip the merged file itself
            if (strcmp(entry->d_name, prefix) == 0) {
                fclose(file);
                continue;
            }

            char buffer[1024];
            size_t bytesRead;

            while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
                fwrite(buffer, 1, bytesRead, mergedFile);
            }

            fclose(file);
        } 

    }

    closedir(dir);
    fclose(mergedFile);

    dir = opendir(directoryPath);

	//remove useless file
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            if (strncmp(entry->d_name, prefix, prefixLength) == 0) {
                char extension[1000];
                sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));
                if (strstr(entry->d_name, extension) != NULL) {
                    sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
                    if (strcmp(filePath, mergedFilePath) != 0) {
                        remove(filePath);
                    }
                }
            }
        }
    }

    closedir(dir);
}
```
Berikut adalah penjelasan dari setiap bagian dalam fungsi tersebut:

- DIR *dir; struct dirent *entry; char filePath[1000]; char mergedFilePath[1000]; FILE *mergedFile; size_t prefixLength = strlen(prefix);: Variabel dir digunakan untuk menyimpan pointer ke direktori yang akan diproses. Variabel entry digunakan untuk membaca setiap entri di dalam direktori. Variabel filePath dan mergedFilePath digunakan untuk menyimpan path lengkap dari file-file kecil dan file besar yang akan digabungkan. Variabel mergedFile adalah pointer yang akan digunakan untuk membuka file besar dalam mode "w" (write). Variabel prefixLength menyimpan panjang dari prefix yang digunakan untuk mencocokkan nama file.

- dir = opendir(directoryPath);: Membuka direktori yang diberikan dalam parameter directoryPath dengan menggunakan opendir.

- sprintf(mergedFilePath, "%s/%s", directoryPath, prefix); mergedFile = fopen(mergedFilePath, "w");: Membentuk path lengkap untuk file besar dengan menggabungkan directoryPath dan prefix. Path ini akan digunakan untuk membuka file besar yang akan digabungkan.

- while ((entry = readdir(dir)) != NULL) { ... }: Melakukan loop untuk membaca setiap entri di dalam direktori.

- if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) { ... }: Memeriksa apakah entri saat ini adalah file (DT_REG) dan nama file tersebut memiliki prefix yang sesuai dengan prefix. Jika iya, maka file tersebut adalah file kecil yang akan digabungkan.

- sprintf(filePath, "%s/%s", directoryPath, entry->d_name); FILE *file = fopen(filePath, "r");: Membentuk path lengkap untuk file kecil saat ini dengan menggabungkan directoryPath dan nama file kecil yang sedang diproses. File ini akan dibuka dalam mode "r" (read) untuk membaca isinya.

- if (strcmp(entry->d_name, prefix) == 0) { fclose(file); continue; }: Memeriksa apakah file yang sedang diproses adalah file besar yang sama dengan prefix. Jika iya, maka file tersebut adalah file gabungan itu sendiri dan tidak perlu digabungkan. File tersebut akan ditutup dan loop akan melanjutkan ke entri berikutnya.

- char buffer[1024]; size_t bytesRead; while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) { fwrite(buffer, 1, bytesRead, mergedFile); }: Membaca data dari file kecil dan menuliskannya ke file besar. Data akan dibaca dalam buffer sebesar 1024 byte (buffer) dan ditulis ke file besar menggunakan fwrite.

- Setelah loop selesai, file kecil saat ini (file) akan ditutup.

- closedir(dir); fclose(mergedFile);: Menutup direktori dan file besar setelah selesai.

- dir = opendir(directoryPath);: Membuka direktori kembali untuk memproses entri berikutnya.

- while ((entry = readdir(dir)) != NULL) { ... }: Melakukan loop lagi untuk membaca entri di dalam direktori.

- if (entry->d_type == DT_REG) { if (strncmp(entry->d_name, prefix, prefixLength) == 0) { ... } }: Memeriksa apakah entri saat ini adalah file (DT_REG) dan memiliki prefix yang sesuai dengan prefix. Jika iya, maka file tersebut adalah file kecil yang tidak akan digabungkan.

- sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));: Membentuk ekstensi file kecil dengan memotong angka setelah prefix dan memformatnya sebagai tiga digit dengan "%03d". Ekstensi ini digunakan untuk memeriksa apakah file kecil tersebut adalah file yang telah digabungkan sebelumnya.

- if (strstr(entry->d_name, extension) != NULL) { ... }: Memeriksa apakah ekstensi file saat ini (entry->d_name) mengandung ekstensi yang sama dengan extension. Jika iya, maka file tersebut adalah file kecil yang telah digabungkan sebelumnya dan perlu dihapus.

- sprintf(filePath, "%s/%s", directoryPath, entry->d_name); if (strcmp(filePath, mergedFilePath) != 0) { remove(filePath); }: Membentuk path lengkap untuk file kecil saat ini dan memeriksa apakah path tersebut sama dengan path file besar (mergedFilePath). Jika tidak sama, maka file kecil tersebut dihapus menggunakan remove.

- Setelah loop selesai, direktori akan ditutup kembali dengan closedir(dir).

#### 5. Full code
```
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/wait.h>
#define CHUNK 1024 //approximate target size of small file

static  const  char * dirPath = "/home/anneutsabita/SISOP/praktikum4/soal4";

char kode[10] = "module_";

void logSystem(char* c, int type){
    FILE * logFile = fopen("/home/anneutsabita/SISOP/praktikum4/soal4/fs_module.log", "a");
	time_t currTime;
	struct tm * time_info;
	time ( &currTime );
	time_info = localtime (&currTime);
    int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;
    if(type==1){//report type
        fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    else if(type==2){ //flag type
        fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
    }
    fclose(logFile);
}

int segment_exists(const char* filename) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL) {
        return 0;  // File does not exist
    }
    fclose(file);
    return 1;  // File exists
}

long file_size(char *name){
    FILE *fp = fopen(name, "rb"); //must be binary read to get bytes
    // printf("%s\n",name);
    long size=-1;
    if(fp)
    {
        fseek (fp, 0, SEEK_END);
        size = ftell(fp)+1;
        fclose(fp);
    }
    return size;
}

void decode(const char *directoryPath, const char *prefix) {
    DIR *dir;
    struct dirent *entry;
    char filePath[1000];
    char mergedFilePath[1000];
    FILE *mergedFile;
    size_t prefixLength = strlen(prefix);

    dir = opendir(directoryPath);

    sprintf(mergedFilePath, "%s/%s", directoryPath, prefix);
    mergedFile = fopen(mergedFilePath, "w");

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) {
            sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
            FILE *file = fopen(filePath, "r");

			// Skip the merged file itself
            if (strcmp(entry->d_name, prefix) == 0) {
                fclose(file);
                continue;
            }

            char buffer[1024];
            size_t bytesRead;

            while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
                fwrite(buffer, 1, bytesRead, mergedFile);
            }

            fclose(file);
        } 

    }

    closedir(dir);
    fclose(mergedFile);

    dir = opendir(directoryPath);

	//remove useless file
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            if (strncmp(entry->d_name, prefix, prefixLength) == 0) {
                char extension[1000];
                sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));
                if (strstr(entry->d_name, extension) != NULL) {
                    sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
                    if (strcmp(filePath, mergedFilePath) != 0) {
                        remove(filePath);
                    }
                }
            }
        }
    }

    closedir(dir);
}

void encrypt(char* enc1, char * enc2){
    if(strcmp(enc1, ".") == 0) return;
    if(strcmp(enc1, "..") == 0)return;
    
    int chunks=0, i, accum;

    char largeFileName[200];    //change to your path
    sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);
    printf("%s\n",largeFileName);

    char filename[260];//base name for small files.
    sprintf(filename,"%s.",largeFileName);
    //printf("%s\n",filename);

    char smallFileName[300];
    char line[1080];
    FILE *fp1, *fp2;
    long sizeFile = file_size(largeFileName);
    // printf("File size : %ld\n",sizeFile);
    
	if (sizeFile > 1025){
		chunks = sizeFile/CHUNK + 1;//ensure end of file
		// printf("%d",chunks);
		fp1 = fopen(largeFileName, "r");
		if(fp1)
		{
			char number[5];
			printf("Splitting\n");
			for(i=0;i<chunks;i++)
			{
				accum = 0;
				sprintf(number,"%03d",i+1);
				
				sprintf(smallFileName, "%s%s", filename, number);

				// Check if segment file already exists, skip if it does
				if (segment_exists(smallFileName)) {
					continue;
				}
				
				fp2 = fopen(smallFileName, "wb");
				if (fp2) {
					while (accum < CHUNK && !feof(fp1)) {
						size_t bytesRead = fread(line, 1, sizeof(line), fp1);
						size_t bytesToWrite = bytesRead;
						if (accum + bytesToWrite > CHUNK) {
							bytesToWrite = CHUNK - accum;
						}
						fwrite(line, 1, bytesToWrite, fp2);
						accum += bytesToWrite;
					}
					fclose(fp2);
				}
			}
			fclose(fp1);
			// Hapus file asli setelah pemecahan selesai
			if (remove(largeFileName) != 0) {
				printf("Gagal menghapus file asli: %s\n", largeFileName);
			} else {
				printf("File asli dihapus: %s\n", largeFileName);
			}
		}
	}
    printf("keluar\n");
}

void filter(char * enc1){
    if(strcmp(enc1, ".") == 0) return;
    if(strcmp(enc1, "..") == 0)return;
	if(strstr(enc1, "/") == NULL)return;
}

static  int  xmp_getattr(const char *path, struct stat *stbuf){
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }
	char newPath[1000];
	int res;
	sprintf(newPath,"%s%s", dirPath, path);
	res = lstat(newPath, stbuf);
	if (res == -1)
		return -errno;
	return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){ //baca directory
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);

	int res = 0;
	struct dirent *dir;
	DIR *dp;
	(void) fi;
	(void) offset;
	dp = opendir(newPath);
	if (dp == NULL) return -errno;

	while ((dir = readdir(dp)) != NULL) { //buat loop yang ada di dalem directory
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if(enc2 != NULL){
			encrypt(dir->d_name, enc2);
        } else {
			char *extension = strrchr(dir->d_name, '.');
            if (extension != NULL && strcmp(extension, ".001") == 0) {
                *extension = '\0';
                decode(newPath, dir->d_name);
            }
		}
		res = (filler(buf, dir->d_name, &st, 0));
		if(res!=0) break;
	}

	closedir(dp);
	return 0;
}

static int xmp_mkdir(const char *path, mode_t mode){ //buat bikin directory baru

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	}
	else sprintf(newPath, "%s%s",dirPath,path);

	int res = mkdir(newPath, mode);
    char str[100];
	sprintf(str, "MKDIR::%s", path);
	logSystem(str,1);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){//buat file

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path = dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);
	int res;

	if (S_ISREG(mode)) {
		res = open(newPath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (res >= 0)
			res = close(res);
	} else if (S_ISFIFO(mode))
		res = mkfifo(newPath, mode);
	else
		res = mknod(newPath, mode, rdev);
    char str[100];
	sprintf(str, "CREATE::%s", path);
	logSystem(str,1);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_unlink(const char *path) { //ngehapus file
	printf("unlink\n");
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);
    char str[100];
	sprintf(str, "REMOVE::%s", path);
	logSystem(str,2);
	int res;
	res = unlink(newPath);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rmdir(const char *path) {//ngehapus directory
	printf("rmdir\n");
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) {
        filter(enc2);
    }

	char newPath[1000];
	sprintf(newPath, "%s%s",dirPath,path);
    char str[100];
	sprintf(str, "RMDIR::%s", path);
	logSystem(str,2);
	int res;
	res = rmdir(newPath);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_rename(const char *from, const char *to) { //buat renme
	printf("\nRENAME!!!\n");
	char fileFrom[1000],fileTo[1000];
	sprintf(fileFrom,"%s%s",dirPath,from);
	sprintf(fileTo,"%s%s",dirPath,to);

    char str[100];
	sprintf(str, "RENAME::%s::%s", from, to);
	logSystem(str,1);
	int res;
	res = rename(fileFrom, fileTo);
	if (res == -1)
		return -errno;

	return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi){ //open file
	char newPath[1000];
	sprintf(newPath, "%s%s",dirPath,path);
	int res;
	res = open(newPath, fi->flags);
	if (res == -1)
		return -errno;
	close(res);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){ //read file
	
	char newPath[1000];
	sprintf(newPath, "%s%s",dirPath,path);
	int fd;
	int res;

	(void) fi;
	fd = open(newPath, O_RDONLY);
	if (fd == -1)
		return -errno;
	res = pread(fd, buf, size, offset);
	if (res == -1)
		res = -errno;
	close(fd);
	return res;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { //write file
	
	char newPath[1000];
	sprintf(newPath, "%s%s", dirPath, path);
	
    int fd;
	int res;
	(void) fi;
	fd = open(newPath, O_WRONLY);
	if (fd == -1)
		return -errno;
    char str[100];
	sprintf(str, "WRITE::%s", path);
	logSystem(str,1);
	res = pwrite(fd, buf, size, offset);
	if (res == -1)
		res = -errno;
	close(fd);
	return res;
}

static struct fuse_operations xmp_oper = {

	.getattr = xmp_getattr,
	.readdir = xmp_readdir,
	.read = xmp_read,
	.mkdir = xmp_mkdir,
	.mknod = xmp_mknod,
	.unlink = xmp_unlink,
	.rmdir = xmp_rmdir,
	.rename = xmp_rename,
	.open = xmp_open,
	.write = xmp_write,

};

int  main(int  argc, char *argv[]){
	umask(0);

	return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

#### Output
![messageImage_1685801542487](/uploads/c4bc9a1f7e15db13a0240f751bd35069/messageImage_1685801542487.jpg)


## Question 5  
Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.					
- Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.

- Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.

-Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. 

- Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.
Contoh:
kyunkyun;fbc5ccdf7c34390d07b1f4b74958a9ce
**Penjelasan:**
kyunkyun adalah username.
fbc5ccdf7c34390d07b1f4b74958a9ce adalah MD5 Hashing dari Subarukun, user kyunkyun menginputkan Subarukun sebagai password. Untuk lebih jelas, kalian bisa membaca dokumentasinya pada https://www.md5hashgenerator.com/. 
**Catatan: **
Tidak perlu ada password validation untuk mempermudah kalian.		
Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi <Nama_Folder>_<Kode_Kelompok> dan <Kode_Kelompok>_<Nama_File>.<ext>.
**Contoh:**
A01_a.pdf
xP4UcxRZE5_A01


List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.
**Contoh: **
result.txt


extension.txt
folder = 12
jpg = 13
png = 2
.
.
.
dan seterusnya

Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.					
Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.
Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.
Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.
Contoh:
kyunkyun;fbc5ccdf7c34390d07b1f4b74958a9ce
Penjelasan:
kyunkyun adalah username.
fbc5ccdf7c34390d07b1f4b74958a9ce adalah MD5 Hashing dari Subarukun, user kyunkyun menginputkan Subarukun sebagai password. Untuk lebih jelas, kalian bisa membaca dokumentasinya pada https://www.md5hashgenerator.com/. 
Catatan: 
Tidak perlu ada password validation untuk mempermudah kalian.		
Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi <Nama_Folder>_<Kode_Kelompok> dan <Kode_Kelompok>_<Nama_File>.<ext>.
Contoh:
A01_a.pdf
xP4UcxRZE5_A01


List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.
Contoh: 
result.txt


extension.txt
folder = 12
jpg = 13
png = 2
.
.
.
dan seterusnya


## Penjelasan
- Pertama, kita mendownload file rahasia.zip pada link yang ada di soal.

void download_file(const char *url, const char *outputPath){   

    char command[1000];
    sprintf(command, "wget \"%s\" -O \"%s\"", url, outputPath);
    system(command);
}

- Kemudian, unzip file zip yang telah didownload.

void unzip_file(const char *outputPath){

    char command[256];
    //sekalian hapus file zip nya karena sudah tidak digunakan
    sprintf(command, "unzip -d . %s && rm rahasia.zip", outputPath);
    system(command);
}


- Setelah folder rahasia didownload, kita duplicate folder tersebut menjadi folder temp yang isinya folder rahasia agar menjadi folder yang berisi rename file.

//copy folder ke /tmp karena dibuat untuk rename nantinya
void copyFolder(){
    system("mkdir -p tmp");

    system("cp -R rahasia tmp");
}

- Melakukan mount ke docker container. Disini, kita mengisi volumes pada docker-compose yang akan berisi path dari folder rahasia (sebelum di rename) dan folder tmp (setelah direname). Kita gunakan juga ubuntu focal fossa serta penamaan image adalah rahasia_di_docker_a01

  version: "3"
  services:
    myapp:
      build:
        context: .
        dockerfile: Dockerfile
      image: rahasia_di_docker_a01
      container_name: container_rahasia
      command: tail -f /dev/null
      volumes:
        - /home/dhe/Documents/Dimas/Programming/sisop/Praktikum4/rahasia:/usr/before
        - /home/dhe/Documents/Dimas/Programming/sisop/Praktikum4/tmp/rahasia:/usr/share


- Isi Dockerfile

FROM ubuntu:focal

RUN mkdir -p /usr/share && mkdir -p /usr/before


# Tambahkan perintah-perintah instalasi atau konfigurasi tambahan jika diperlukan

CMD tail -f /dev/null

- Kita buat register system yang menyimpan username dan password lalu password menggunakan hashing MD5. Disini, kami simpan kredensial ke local.

- Sebelum masuk register system:
if(pid2 == 0){
    //hak akses default tidak akan berubah
    umask(0);

    //alamat dari struktur xmp_oper yang berisi implementasi dari operasi-operasi filesystem
    //NULL berarti tidak ada data tambahan yang diteruskan ke filesystem
    fuse_main(argc, argv, &xmp_oper, NULL);
}

else{
    
    //kita pull imagenya dulu lalu build docker-compose
    system("docker pull ubuntu:focal");
    system("docker-compose up -d");

- Setelah kita build docker image dan container, kita masuk ke register system.
system("sudo touch users.txt");
system("sudo chmod 777 users.txt");

printf("Welcome to main menu!!\n");
printf("Please choose between login or register (input: 'login' or 'register'): ");

char temp[20];
scanf("%s", temp);

if (strcmp(temp, "register") == 0) {
    regis();
} 

else if(strcmp(temp, "login") == 0) {
    login();
}

else{

    while (strcmp(temp, "login") != 0 && strcmp(temp, "register") != 0) {

        printf("Invalid choice! Please enter either 'login' or 'register': ");
        scanf("%s", temp);
    }

    if (strcmp(temp, "login") == 0) {

        login();
    } 
    
    else if (strcmp(temp, "register") == 0) {
        regis();
    }
}


- Sistem regis:
//regis
void regis(){

    puts("Welcome to Register Menu");

    char username[100];
    printf("Enter your new username: ");
    scanf("%s", username);
    username[strcspn(username, "\n")] = '\0';
    
    //cek apakah user yang dimasukkan berbeda
    while (isUsernameAvail(username)) {
        
        printf("Username is already taken. Please enter a different username: ");
        scanf("%s", username);
        username[strcspn(username, "\n")] = '\0';
    }
    
    char password[100];
    printf("Enter your new password: ");
    char* input = getpass("");
    strncpy(password, input, 100);

    //hapus input yang tersimpan di memori
    memset(input, 0, strlen(input));

    //membuat jadi md5 hashing
    char hashed_password[500];
    strcpy(hashed_password, GetMD5String(password, sizeof(password)));

    //menyimpan ke users.txt
    registerUser(username, hashed_password);

    printf("Registration successful. Username and password have been saved.\n");

    char choice[20];
    printf("Choose an option 'login' or 'done' : ");
    scanf("%s", choice);

    if (strcmp(choice, "login") == 0) {
        login();
    }

};


- Cek apakah username available atau tidak di isUsernameAvail
//cek apakah user available
int isUsernameAvail(const char *username) {
    FILE *file = fopen("users.txt", "r");
    if (file == NULL) {
        printf("Failed to open users.txt\n");
        exit(1);
    }

    char line[100];
    while (fgets(line, sizeof(line), file)) {
        //cek apakah sama dengan username
        char *found = strstr(line, username);
        if (found != NULL) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);

    return 0;
}

- Mengubah password menjadi hashing MD5
//kode hashing md5 dari google
typedef union uwb {
	unsigned w;
	unsigned char b[4];
} MD5union;

typedef unsigned DigestArray[4];

unsigned func0(unsigned abcd[]) {
	return (abcd[1] & abcd[2]) | (~abcd[1] & abcd[3]);
}

unsigned func1(unsigned abcd[]) {
	return (abcd[3] & abcd[1]) | (~abcd[3] & abcd[2]);
}

unsigned func2(unsigned abcd[]) {
	return  abcd[1] ^ abcd[2] ^ abcd[3];
}

unsigned func3(unsigned abcd[]) {
	return abcd[2] ^ (abcd[1] | ~abcd[3]);
}

typedef unsigned(*DgstFctn)(unsigned a[]);

unsigned *calctable(unsigned *k)
{
	double s, pwr;
	int i;

	pwr = pow(2.0, 32);
	for (i = 0; i<64; i++) {
		s = fabs(sin(1.0 + i));
		k[i] = (unsigned)(s * pwr);
	}
	return k;
}

unsigned rol(unsigned r, short N)
{
	unsigned  mask1 = (1 << N) - 1;
	return ((r >> (32 - N)) & mask1) | ((r << N) & ~mask1);
}

unsigned* Algorithms_Hash_MD5(const char *msg, int mlen)
{
	static DigestArray h0 = { 0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476 };
	static DgstFctn ff[] = { &func0, &func1, &func2, &func3 };
	static short M[] = { 1, 5, 3, 7 };
	static short O[] = { 0, 1, 5, 0 };
	static short rot0[] = { 7, 12, 17, 22 };
	static short rot1[] = { 5, 9, 14, 20 };
	static short rot2[] = { 4, 11, 16, 23 };
	static short rot3[] = { 6, 10, 15, 21 };
	static short *rots[] = { rot0, rot1, rot2, rot3 };
	static unsigned kspace[64];
	static unsigned *k;

	static DigestArray h;
	DigestArray abcd;
	DgstFctn fctn;
	short m, o, g;
	unsigned f;
	short *rotn;
	union {
		unsigned w[16];
		char     b[64];
	}mm;
	int os = 0;
	int grp, grps, q, p;
	unsigned char *msg2;

	if (k == NULL) k = calctable(kspace);

	for (q = 0; q<4; q++) h[q] = h0[q];

	{
		grps = 1 + (mlen + 8) / 64;
		msg2 = (unsigned char*)malloc(64 * grps);
		memcpy(msg2, msg, mlen);
		msg2[mlen] = (unsigned char)0x80;
		q = mlen + 1;
		while (q < 64 * grps) { msg2[q] = 0; q++; }
		{
			MD5union u;
			u.w = 8 * mlen;
			q -= 8;
			memcpy(msg2 + q, &u.w, 4);
		}
	}

	for (grp = 0; grp<grps; grp++)
	{
		memcpy(mm.b, msg2 + os, 64);
		for (q = 0; q<4; q++) abcd[q] = h[q];
		for (p = 0; p<4; p++) {
			fctn = ff[p];
			rotn = rots[p];
			m = M[p]; o = O[p];
			for (q = 0; q<16; q++) {
				g = (m*q + o) % 16;
				f = abcd[1] + rol(abcd[0] + fctn(abcd) + k[q + 16 * p] + mm.w[g], rotn[q % 4]);

				abcd[0] = abcd[3];
				abcd[3] = abcd[2];
				abcd[2] = abcd[1];
				abcd[1] = f;
			}
		}
		for (p = 0; p<4; p++)
			h[p] += abcd[p];
		os += 64;
	}
	return h;
}

const char* GetMD5String(const char *msg, int mlen) {
	char str[33];
	strcpy(str, "");
	int j;
	unsigned *d = Algorithms_Hash_MD5(msg, strlen(msg));
	MD5union u;
	for (j = 0; j<4; j++) {
		u.w = d[j];
		char s[9];
		sprintf(s, "%02x%02x%02x%02x", u.b[0], u.b[1], u.b[2], u.b[3]);
		strcat(str, s);
	}

	return strdup(str);
}


- Menyimpan username dan password baru ke user.txt dalam fungsi registerUser
//menyimpan user baru
void registerUser(const char *username, const char *password) {
    FILE *file = fopen("users.txt", "a");
    if (file == NULL) {
        printf("Failed to open users.txt\n");
        exit(1);
    }

    fprintf(file, "%s;%s\n", username, password);
    fclose(file);
}


- Sistem login:
//login
void login(){
    
    puts("Welcome to Login Menu");

    char username[100];
    char password[100];
    char hashed_password[500];

    int isAuthenticated = 0;

    do {
        printf("Enter your username: ");
        scanf("%s", username);
        username[strcspn(username, "\n")] = '\0';
        
        printf("Enter your new password: ");
        //agar tidak muncul diterminal
        char* input = getpass("");
        strncpy(password, input, 100);

        //hapus input yang tersimpan di memori
        memset(input, 0, strlen(input));

        //menyimpan hasil md5 ke hashed_password
        strcpy(hashed_password, GetMD5String(password, sizeof(password)));

        if (checkCredentials(username, hashed_password)) {
            isAuthenticated = 1;
            printf("Login successful!\n");

            akses();
        } 
        else {
            printf("Invalid username or password. Please try again.\n");
        }
    } 
    while (!isAuthenticated);


};

- Cek apakah username dan password sesuai dalam users.txt
//cek credential untuk login dan register

int checkCredentials(const char* username, const char* password) {
    FILE* file = fopen("users.txt", "r");
    if (file == NULL) {
        printf("Failed to open users.txt.\n");
        return 0;
    }

    char line[100];
    char storedUsername[100];
    char storedPassword[500];
    int isAuthenticated = 0;

    while (fgets(line, sizeof(line), file)) {
        //cek apakah username dan password sesuai dalam users.txt
        sscanf(line, "%[^;];%s", storedUsername, storedPassword);
        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedPassword) == 0) {
            isAuthenticated = 1;
            break;
        }
    }

    fclose(file);
    return isAuthenticated;
}


- Jika login berhasil, maka user dapat melakukan berbagai akses yaitu akses read (sebelum di rename) dan akses rename (semua folder isinya telah di rename)

//hal yang didapatkan user saat sudah login
void akses(){

    char jawaban[20];
    while (1) {
        printf("Hello, What do you want to do? (Input: 'read' or 'rename' or 'exit'): ");
        scanf("%s", jawaban);

        //dapat langsung memunculkan isi dari folder rahasia yang disimpan dalam /usr/before
        if (strcmp(jawaban, "read") == 0) {
            system("docker exec -it container_rahasia /bin/bash -c 'ls /usr/before'");
        } 

        //memunculkan isi dari folder rahasia yang telah direname dan tersimpan dalam /usr/share
        else if (strcmp(jawaban, "rename") == 0) {
            system("docker exec -it container_rahasia /bin/bash -c 'ls /usr/share'");  
        } 

        else if(strcmp(jawaban, "exit") == 0){
            break;
        }
        
        else {
            printf("Invalid choice. Please try again.\n");
        }
    }
}


- Untuk melakukan rename, kita gunakan fuse. Disini, kita mengubahnya melalui xmp_readdir

//read directory
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } 
    
    else sprintf(fpath, "%s%s", dirpath, path);

    // printf("Debug: Calling readdir for path: %s\n", fpath); // Menampilkan path yang sedang diproses

    int res = 0;

    DIR *dp;
    //informasi tentang entri dalam directory
    struct dirent *de;

    //memberikan peringatan terkait variabel yang tidak digunakan pada kompilasi
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        printf("Debug: Failed to open directory: %s\n", fpath); // Menampilkan pesan jika gagal membuka direktori
        return -errno;
    }

    //membaca setiap entry dalam dp ke dirent de
    while ((de = readdir(dp)) != NULL) {
        //menyimpan atribut dari entri dirent yang sedang dibaca
        struct stat st;
        //seluruh byte dibuat 0
        memset(&st, 0, sizeof(st));

        //inode dari st berisi inode dari de
        st.st_ino = de->d_ino;
        //mengambil tipe file 
        st.st_mode = de->d_type << 12;

        char entry_path[5000];
        sprintf(entry_path, "%s/%s", fpath, de->d_name);

        //memeriksa status file untuk mendapatkan informasi
        if (stat(entry_path, &st) == -1) {
            // printf("Debug: Failed to get file information for: %s\n", entry_path); // Menampilkan pesan jika gagal mendapatkan informasi berkas
            return -errno;
        }
        
        char new_entry_path[5000];
        // cek apakah termasuk directory
        if (S_ISDIR(st.st_mode)) {

            //memeriksa apakah directory yang sedang diproses bukanlah directory saat ini atau directory induk
            if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0) {

                char new_name[2000];
                sprintf(new_name, "%s_A01", de->d_name);

                // Mengecek apakah kata 'A01' sudah ada dalam nama folder sebelum melakukan rename
                if (strstr(de->d_name, "A01") == NULL) {
                    // Melakukan rename folder
                    sprintf(entry_path, "%s/%s", fpath, de->d_name);
                    sprintf(new_entry_path, "%s/%s", fpath, new_name);
                    if (rename(entry_path, new_entry_path) == -1) {
                        // printf("Debug: Failed to rename folder: %s\n", entry_path);
                    }

                    res = xmp_readdir(entry_path, buf, filler, offset, fi);
                }
            }

        }
        //cek apakah termasuk file
        if(S_ISREG(st.st_mode)) {
            
            //apakah nama file mengandung "A01"
            if (strstr(de->d_name, "A01") == NULL) {
                char new_name[2000];
                //pointer ke titik terakhir. jadi ext isinya bakal .txt or sth else
                char *ext = strrchr(de->d_name, '.');
                if (ext != NULL) {
                    // *ext = '\0'; // Remove the file extension from the name
                    sprintf(new_name, "A01_%s", de->d_name);

                    char new_entry_path[5000];
                    char old_entry_path[5000];
                    sprintf(old_entry_path, "%s/%s", fpath, de->d_name);

                    sprintf(new_entry_path, "%s/%s", fpath, new_name);

                    // Memeriksa keberadaan file sebelum melakukan rename
                    if (access(new_entry_path, F_OK) == -1) {
                        // File dengan nama yang diinginkan belum ada, lanjutkan dengan proses rename
                        if (rename(entry_path, new_entry_path) == -1) {
                            // printf("Debug: Failed to rename file: %s\n", entry_path);
                            perror("Error"); // Menampilkan pesan error lebih spesifik
                        }
                    } else {
                        // printf("Debug: File with desired name already exists: %s\n", new_entry_path);
                        // Tambahkan log atau penanganan kesalahan sesuai kebutuhan
                    }
                }    
            }
        }

        //mengisi buffer dan memastikan buffer tidak penuh
        /**
         *      buf: Buffer yang digunakan untuk menyimpan informasi mengenai entri dirent.
                de->d_name: Nama dari entri dirent yang sedang dibaca.
                &st: Alamat dari struktur st yang berisi atribut (stat) dari entri dirent tersebut.
                0: Nilai offset yang digunakan untuk menentukan posisi entri dirent dalam buffer. Dalam hal ini, nilai 0 menunjukkan bahwa entri dirent akan dimasukkan ke dalam buffer pada posisi saat ini.
        */

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0) break;
    }

    closedir(dp);

    // printf("Debug: readdir completed for path: %s\n", fpath); // Menampilkan pesan setelah selesai membaca direktori

    return 0;
}

-  Kita juga memiliki fungsi xmp_rename untuk melakukan rename

//melakukan rename
static int xmp_rename(const char *from, const char *to)
{
    char fpathFrom[1000];
    char fpathTo[1000];

    sprintf(fpathFrom, "%s%s", dirpath, from);
    sprintf(fpathTo, "%s%s", dirpath, to);

    printf("rename: %s to %s\n", fpathFrom, fpathTo);

    //terjadi rename
    int res = rename(fpathFrom, fpathTo);
    if (res == -1)
        return -errno;

    return 0;
}

- e. Kita masukkan fungsi xmp_rename tersebut ke fuse_operations

//menentukan operasi-operasi yang akan dilakukan pada filesystem yang sedang dibuat
static struct fuse_operations xmp_oper = {
    //mendapatkan atribut (metadata) dari file atau directory
    .getattr = xmp_getattr,
    //membaca isi dari sebuah direktori
    .readdir = xmp_readdir,
    //membaca isi dari sebuah file
    .read = xmp_read,
    //melakukan rename
    .rename = xmp_rename,
    //untuk membuat tree
    .open = xmp_open,
};


- Kita lakukan list seluruh folder, subfolder, dan file yang telah di rename dalam file result.txt menggunakan tree
Gunakan xmp_open


//mount path ini digunakan untuk menjalankan command tree
static  const  char *mount_path = "/home/dhe/Documents/Dimas/Programming/sisop/Praktikum4/tmp/rahasia";

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void) fi;

    // Buka file result.txt untuk dituliskan output dari perintah "tree"
    /**
     * status:
     * O_WRONLY: mode write only
     * O_CREAT: file akan dibuat jika belum ada
     * O_TRUNC: file akan dipotong (truncate) jika sudah ada sebelumnya
     * 0664: hak akses read write untuk pemilik file
    */
    int fd = open("/home/dhe/Documents/Dimas/Programming/sisop/Praktikum4/result.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (fd == -1) {
        perror("Error opening file");
        return -1;
    }

    // Menyimpan stdout yang sebelumnya digunakan untuk mencetak ke konsol
    int simpan_stdout = dup(STDOUT_FILENO);

    // Mengarahkan stdout ke file result.txt
    if (dup2(fd, STDOUT_FILENO) == -1) {
        perror("Error redirecting stdout");
        close(fd);
        return -1;
    }

    // Menjalankan perintah "tree" pada direktori yang telah dimount
    char command[1024];
    snprintf(command, sizeof(command), "tree %s", mount_path);

    system(command);

    // Mengembalikan stdout ke keadaan semula
    dup2(simpan_stdout, STDOUT_FILENO);
    close(simpan_stdout);

    close(fd);

    return 0;
}


Lalu kita masukkan xmp_open ke fuse_operations:


//menentukan operasi-operasi yang akan dilakukan pada filesystem yang sedang dibuat
static struct fuse_operations xmp_oper = {
    //mendapatkan atribut (metadata) dari file atau directory
    .getattr = xmp_getattr,
    //membaca isi dari sebuah direktori
    .readdir = xmp_readdir,
    //membaca isi dari sebuah file
    .read = xmp_read,
    //melakukan rename
    .rename = xmp_rename,
    //untuk membuat tree
    .open = xmp_open,
};


- 8. Kita hitung jumlah file berdasarkan extensionnya

if(pid == 0){ //child process

    download_file(url, outputPath);
}
else{

    wait(NULL);

    //karena folder rahasia gabisa kesimpen kalau ada compiler bernama rahasia
    remove("rahasia");

    //melakukan unzip
    unzip_file(outputPath);

    //melakukan copy folder karena untuk rename
    copyFolder();

    //menghitung jumlah file dan folder
    const char *pathh = "/home/dhe/Documents/Dimas/Programming/sisop/Praktikum4/rahasia";
    countEntities(pathh);

int countfolder = 0;
int countjpg = 0;
int countpdf = 0;
int counttxt= 0;
int countmp3 = 0;
int countpng = 0;
int countdocx = 0;
int countgif = 0;

//untuk rekursi menghitung banyaknya folder dan file
void countEntities(const char *pathh) {
    DIR *dir = opendir(pathh);
    if (dir == NULL) {
        printf("Failed to open directory: %s\n", pathh);
        return;
    }

    struct dirent *entry;
    //mengambil file dalam pathh
    while ((entry = readdir(dir)) != NULL) {
        // Skip "." and ".." entries
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        // Construct the full path of the entry
        char entryPath[1000];
        snprintf(entryPath, sizeof(entryPath), "%s/%s", pathh, entry->d_name);

        struct stat st;
        if (stat(entryPath, &st) == -1) {
            printf("Failed to get file information for: %s\n", entryPath);
            continue;
        }

        //cek apakah merupakan file
        if (S_ISREG(st.st_mode)) {
            if (strstr(entry->d_name, "pdf") != NULL) {
                countpdf++;
            } 
            
            else if (strstr(entry->d_name, "jpg") != NULL) {
                countjpg++;
            } 
            
            else if (strstr(entry->d_name, "txt") != NULL) {
                counttxt++;
            } 
            
            else if (strstr(entry->d_name, "mp3") != NULL) {
                countmp3++;
            } 
            
            else if (strstr(entry->d_name, "png") != NULL) {
                countpng++;
            } 
            
            else if (strstr(entry->d_name, "docx") != NULL) {
                countdocx++;
            } 
            
            else if (strstr(entry->d_name, "gif") != NULL) {
                countgif++;
            } 
            
            else {
                // Extension tidak sesuai dengan yang diharapkan, tambahkan penanganan kesalahan di sini
                printf("Extension tidak dikenali: %s\n", entry->d_name);
            }
        }

        //cek apakah merupakan directory
        else if (S_ISDIR(st.st_mode)) {
            countfolder++;

            // akan terjadi rekursi karena membaca isi dari subfolder
            countEntities(entryPath);
        }
    }

    closedir(dir);
}



- Lalu, kita simpan hasilnya ke extension.txt
const char *pathh = "/home/dhe/Documents/Dimas/Programming/sisop/Praktikum4/rahasia";
countEntities(pathh);

//menyimpan tree
system("sudo touch extension.txt");
system("sudo chmod 777 extension.txt");

FILE *file = fopen("extension.txt", "w");
if (file == NULL) {
    printf("Failed to open file.\n");
    return 1;
}

fprintf(file, "pdf = %d\n", countpdf);
fprintf(file, "jpg = %d\n", countjpg);
fprintf(file, "folder = %d\n", countfolder);
fprintf(file, "txt = %d\n", counttxt);
fprintf(file, "mp3 = %d\n", countmp3);
fprintf(file, "png = %d\n", countpng);
fprintf(file, "docx = %d\n", countdocx);
fprintf(file, "gif = %d\n", countgif);

fclose(file);


## Output
Docker image berhasil diup
![image](/uploads/e524f3ebde0cb1ff4364782d5e013dff/image.png)



- Dapat masuk ke register system setelah docker container di up
![image](/uploads/050e0c7f0847efb6f80ab0f962996344/image.png)


- Username dan Password dari register dapat tersimpan di users.txt
![image](/uploads/5db973ad46bdc6544c1e0a2ee6e74b96/image.png)

- Setelah login, dapat melakukan read
![image](/uploads/a6182503b60587faa194263f46fc9a8d/image.png)

- Setelah login, dapat melakukan rename
![image](/uploads/7cd2b5c1c2a3cc686fd6e8ea45041db0/image.png)

- Mountfolder local telah berubah menjadi folder yang telah di rename. Disini mountpath ke folder rename_rahasia
![image](/uploads/8d2c3d0b9c173528bd7fbf9c4cd8b6d8/image.png)


- Hasil tree dalam result.txt sesuai
/home/dhe/Documents/Dimas/Programming/sisop/Praktikum4/tmp/rahasia
├── 80T86jn7xh_A01
│   ├── A01_elaine-casap-82gJggDId-U-unsplash.jpg
│   └── A01_tobias-tullius-3BcSyNMJ2WM-unsplash.jpg
├── A01_b.pdf
├── A01_carles-rabada-kMde0v9tYYM-unsplash.jpg
├── A01_File-49ynD.txt
├── A01_File-Oadk3.txt
├── A01_File-PdCim.txt
├── A01_funny-eastern-short-music-vlog-background-hip-hop-beat-29-sec-148905.mp3
├── A01_mygamer.gif
├── A01_png-transparent-construction-equipment-illustrations-heavy-machinery-architectural-engineering-truck-vehicle-construction-sites-icon-material-free-free-logo-design-template-building-text-thumbnail.png
├── AG5zGHsasr_A01
│   └── A01_File-iVVm9.txt
├── dU87gUYIBN_A01
│   ├── A01_cinematic-logo-strong-and-wild-142807.mp3
│   ├── A01_clapping-opener-rhythmic-original-dynamic-promo-114789.mp3
│   ├── A01_mohamed-ahsan-cjkgrFt_6bE-unsplash.jpg
│   ├── A01_motivational-corporate-short-110681.mp3
│   ├── A01_png-transparent-free-download-icon-cloud-down-computer-system-blue-computer-blue-laptop-thumbnail.png
│   └── A01_sergey-zhesterev-lutEBBTpkeE-unsplash.jpg
├── Dyy2q9B0Ax_A01
│   ├── A01_File-CxD3o.txt
│   └── A01_patrick-hendry-jS0ysot7MwE-unsplash.jpg
├── FH2CQo5o8z_A01
│   ├── A01_File-dUnQq.txt
│   ├── A01_File-QDJhp.txt
│   ├── A01_File-uuXxv.txt
│   ├── A01_png-transparent-zombie-download-with-transparent-background-fantasy-free-thumbnail.png
│   └── A01_randall-ruiz-LVnJlyfa7Zk-unsplash.jpg
├── gGJ5r0Df82_A01
│   ├── A01_andre-tan-3SK2x-kbMSI-unsplash.jpg
│   ├── A01_andrii-ganzevych-IVlVHXgvi0Y-unsplash.jpg
│   ├── A01_carles-rabada-L7J0q5N7LjQ-unsplash.jpg
│   ├── A01_File-aBxnM.txt
│   ├── A01_File-EBdiV.txt
│   ├── A01_File-OlZ8w.txt
│   └── A01_File-XZJco.txt
├── IKJHSAU9731289dsa_A01
│   └── A01_png-transparent-mobile-phone-repair-material-mobile-phone-repair-phone-iphone-thumbnail.png
├── IzvxA5vw2U_A01
│   ├── A01_kris-mikael-krister-aGihPIbrtVE-unsplash.jpg
│   ├── A01_png-transparent-video-production-freemake-video-er-video-icon-free-angle-text-rectangle-thumbnail.png
│   └── A01_the-devilx27s-entry-143138.mp3
├── m4xOZ1lmux_A01
│   ├── A01_bharat-patil-_kPuQcU8C-A-unsplash.jpg
│   ├── A01_File-4qjlL.txt
│   ├── A01_File-uNOZN.txt
│   ├── A01_jon-tyson-Fz2BWfLDvGI-unsplash.jpg
│   └── A01_png-transparent-yellow-fire-fire-flame-fire-photography-orange-flame-thumbnail.png
├── mBFLP3XQMv_A01
│   ├── A01_epicaly-short-113909.mp3
│   ├── A01_File-6tNX4.txt
│   ├── A01_jcob-nasyr-uGPBqF1Yls0-unsplash.jpg
│   ├── A01_j.docx
│   ├── A01_ramona-flwrs-uJjhnN2WQJs-unsplash.jpg
│   ├── A01_redd-f-t8ts5bNQyWo-unsplash.jpg
│   └── A01_sami-takarautio-JiqalEW6Ml0-unsplash.jpg
├── nkSVqqpV7o_A01
│   ├── A01_c.pdf
│   ├── A01_d.pdf
│   ├── A01_epic-sport-clap-ampampamp-loop-main-9900.mp3
│   ├── A01_File-7Yf0S.txt
│   ├── A01_File-p6ZH2.txt
│   ├── A01_File-xBUoT.txt
│   ├── A01_k-on-yui-hirasawa.gif
│   ├── A01_matthew-feeney-L35x5fU07hY-unsplash.jpg
│   ├── A01_simple-logo-149190.mp3
│   ├── A01_thomas-owen-Y8vui_5mdto-unsplash.jpg
│   └── A01_upbeat-future-bass-opener-136069.mp3
├── Pp9LXs5FkR_A01
│   ├── A01_clapping-music-for-typographic-video-version-2-112975.mp3
│   ├── A01_File-UWKAJ.txt
│   ├── A01_File-X8lyf.txt
│   ├── A01_honkai-impact3rd-cute.gif
│   └── A01_wexor-tmg-L-2p8fapOA8-unsplash.jpg
├── SaEixBbm6y_A01
│   ├── A01_g.pdf
│   ├── A01_haikyuu-anime.gif
│   └── A01_h.pdf
├── uuXVhCbP3X_A01
│   ├── A01_a.pdf
│   ├── A01_e.pdf
│   ├── A01_epic-background-music-for-video-dramatic-hip-hop-24-seconds-149629.mp3
│   ├── A01_png-transparent-blue-dna-illustration-dna-3d-rendering-3d-computer-graphics-free-to-pull-dna-material-3d-computer-graphics-free-logo-design-template-photography-thumbnail.png
│   └── A01_png-transparent-tableware-plate-white-ceramic-plate-platter-dishware-download-with-transparent-background-thumbnail.png
├── VOpdACXtF3_A01
│   ├── A01_heart-ritsu-tainaka.gif
│   ├── A01_png-transparent-moon-moon-image-file-formats-nature-moon-png-thumbnail.png
│   └── A01_png-transparent-snowflake-light-snowflake-free-glass-free-logo-design-template-symmetry-thumbnail.png
├── vZAZecJwKF_A01
│   ├── A01_i.docx
│   ├── A01_png-transparent-kiwifruit-kiwi-free-fruit-kiwi-s-watercolor-painting-image-file-formats-food-thumbnail.png
│   ├── A01_png-transparent-sun-the-sun-sunscreen-light-sphere-sun-image-file-formats-orange-sphere-thumbnail.png
│   └── A01_tango-background-hip-hop-music-for-video-29-seconds-149879.mp3
├── xP4UcxRZE5_A01
│   ├── A01_energetic-background-reggaeton-short-music-27-sec-fun-vlog-music-149384.mp3
│   ├── A01_f.pdf
│   ├── A01_fun-background-hip-hop-short-music-27-sec-energetic-vlog-music-148916.mp3
│   ├── A01_kiss-hug.gif
│   ├── A01_png-transparent-pearl-material-sphere-pearl-balloon-download-with-transparent-background-free-thumbnail.png
│   ├── A01_png-transparent-yellow-fireworks-illustration-fireworks-pyrotechnics-free-to-pull-the-material-fireworks-s-free-logo-design-template-holidays-poster-thumbnail.png
│   └── A01_sirin-honkai-impact.gif
└── ZlzqBmSYbE_A01
    ├── A01_File-nENTg.txt
    ├── A01_File-NWnoK.txt
    ├── A01_File-xnlpH.txt
    ├── A01_png-transparent-bubble-illustration-water-drop-rain-drops-angle-white-text-thumbnail.png
    └── A01_png-transparent-golden-pistol-guns-pistol-free-download-pistol-firearms-thumbnail.png

18 directories, 90 files




